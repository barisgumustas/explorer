require 'rubygems'
require 'bundler'

Bundler.require

ROOT = Pathname.new(File.dirname(__FILE__)).freeze
Dir['lib/*'].each { |file| require File.expand_path(file) }
