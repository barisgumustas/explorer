require './boot'

file = ARGV.first

if file.to_s.strip == ''
  puts "No file specified for executing.\nExample usage: ruby run.rb ./test/data/commmands.txt"
elsif File.exist?(file)
  CommandParser.new(IO.read(file), true).execute.each do |rover|
    puts rover
  end
end
