# Explorer
Explorer parses and executes square plateau exploration plans for the latest Mars rovers of NASA.

## Exploration Plan Format
```
5 5
1 2 N
LMLMLMLMM
3 3 E
MMRMMRMRRM
```
* **1st line** Outer bounds of the plateau
* **(2n+2)th line** Landing point of drone
* **(2n+3)th line** Instruction chain for the drone

### Outer Bound Format
`X(Integer) Y(Integer)`

### Landing Point Format
`X(Integer) Y(Integer) D(Direction)`

#### Valid Direction Values:
* N: North
* E: East
* S: South
* W: West

### Instruction Chain Format
Instruction chains are composed of instruction characters following each other where each character represents an instruction which will be executed by the drone.

#### Valid Instruction Characters
* L: Turn Left
* R: Turn Right
* M: Move

## Command Line Usage
Command line tool executes the plan and prints the final coordinates and directions of drones at the end of the execution. 
1. First install the dependencies by running `bundle install`
2. Then execute the run.rb file followed by the path of the plan file. Example: `ruby run.rb ./test/data/plans.txt`