class Rover
  DIRECTIONS    = %w(N E S W).freeze
  MOVEMENT_MAP  = {
    'N' => { x: 0, y: 1 },
    'E' => { x: 1, y: 0 },
    'S' => { x: 0, y: -1 },
    'W' => { x: -1, y: 0 }
  }.freeze

  attr_reader :x, :y, :d

  def initialize(x: 0, y: 0, d: DIRECTIONS.first)
    raise 'Invalid direction' unless DIRECTIONS.include? d
    @x = x
    @y = y
    @d = d
  end

  def turn_right
    @d = DIRECTIONS[(DIRECTIONS.index(@d) + 1) % 4]
    self
  end

  def turn_left
    @d = DIRECTIONS[(DIRECTIONS.index(@d) - 1) % 4]
    self
  end

  def move
    movement_details = MOVEMENT_MAP[@d]
    @x += movement_details[:x]
    @y += movement_details[:y]
    self
  end

  def to_s
    "#{@x} #{@y} #{@d}"
  end
end
