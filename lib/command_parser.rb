class CommandParser
  attr_reader :limits, :plans, :strict

  INSTRUCTION_MAP = {
    'L' => :turn_left,
    'R' => :turn_right,
    'M' => :move
  }.freeze

  def initialize(commands, strict = false)
    lines         = commands.lines
    max_x, max_y  = parse_limits lines.shift

    @limits       = { x: max_x, y: max_y }
    @plans        = lines.each_slice(2)
    @strict       = strict

    @plans.each_with_index do |plan, index|
      raise "Invalid plan for Rover##{index}" if plan.length != 2
    end
  end

  def execute
    @plans.collect do |plan|
      rover         = build_rover_from_plan(plan)
      instructions  = read_instructions_from_plan(plan)

      execute_instructions(rover, instructions)
    end
  end

  private

  def enforce_bounds(rover)
    raise "Rover(#{rover}) out of bounds" if @strict && out_of_bounds?(rover)
    rover
  end

  def out_of_bounds?(rover)
    !rover.x.between?(0, @limits[:x]) || !rover.y.between?(0, @limits[:y])
  end

  def execute_instructions(rover, instructions)
    instructions.reduce(rover) do |step, instruction|
      enforce_bounds step.send(instruction)
    end
  end

  def read_instructions_from_plan(plan)
    plan.last.strip.chars.collect do |instruction_key|
      instruction = INSTRUCTION_MAP[instruction_key]
      instruction.nil? ? raise("Invalid instruction #{instruction_key}") : instruction
    end
  end

  def build_rover_from_plan(plan)
    x, y, d = plan.first.split
    Rover.new x: x.to_i, y: y.to_i, d: d
  end

  def parse_limits(limits_line)
    limits_line.strip.split.collect(&:to_i)
  end
end
