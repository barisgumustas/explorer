require 'simplecov'
SimpleCov.start do
  add_filter '/test/'
  add_filter '/boot.rb'
end if ENV['COVERAGE']

require File.expand_path('../../boot', __FILE__)

Bundler.require :test
MiniTest::Reporters.use! MiniTest::Reporters::DefaultReporter.new(color: true)

require 'minitest/autorun'
