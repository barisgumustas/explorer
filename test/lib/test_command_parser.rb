require File.expand_path('../../env', __FILE__)

class TestCommandParser < Minitest::Test
  SAMPLE_COMMANDS = ROOT.join('test', 'data', 'plan.txt')

  def setup
    commands  = IO.read(SAMPLE_COMMANDS)
    @parser   = CommandParser.new(commands)
  end

  def test_initialize
    assert_equal @parser.limits, x: 5, y: 5
  end

  def test_execute
    results = @parser.execute
    assert_equal results.length, 2
    assert_equal results.first.to_s, '1 3 N'
    assert_equal results.last.to_s, '5 1 E'
  end

  def test_strict_execute
    commands  = "1 1\n0 0 N\nMMMMMMMMMMM"
    parser    = CommandParser.new(commands, true)
    exception = assert_raises { parser.execute }

    assert_equal exception.message, 'Rover(0 2 N) out of bounds'
  end

  def test_invalid_instruction
    commands  = "1 1\n0 0 N\nX"
    parser    = CommandParser.new(commands, true)
    exception = assert_raises { parser.execute }

    assert_equal exception.message, 'Invalid instruction X'
  end

  def test_invalid_plan
    commands = "1 1\n0 0 N"
    exception = assert_raises { CommandParser.new(commands, true) }
    assert_equal exception.message, 'Invalid plan for Rover#0'
  end
end
