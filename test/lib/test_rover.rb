require File.expand_path('../../env', __FILE__)

class TestRover < Minitest::Test
  def test_initialize
    rover = Rover.new(x: 1, y: 1, d: 'S')

    assert_equal rover.x, 1
    assert_equal rover.y, 1
    assert_equal rover.d, 'S'
  end

  def test_move
    rover = Rover.new

    rover.move
    assert_equal rover.x, 0
    assert_equal rover.y, 1
    assert_equal rover.d, 'N'

    rover.turn_right.move
    assert_equal rover.x, 1
    assert_equal rover.y, 1
    assert_equal rover.d, 'E'

    rover.turn_right.move
    assert_equal rover.x, 1
    assert_equal rover.y, 0
    assert_equal rover.d, 'S'

    rover.turn_right.move
    assert_equal rover.x, 0
    assert_equal rover.y, 0
    assert_equal rover.d, 'W'
  end

  def test_turn_right
    rover = Rover.new

    rover.turn_right
    assert_equal rover.d, 'E'

    rover.turn_right
    assert_equal rover.d, 'S'

    rover.turn_right
    assert_equal rover.d, 'W'

    rover.turn_right
    assert_equal rover.d, 'N'
  end

  def test_turn_left
    rover = Rover.new

    rover.turn_left
    assert_equal rover.d, 'W'

    rover.turn_left
    assert_equal rover.d, 'S'

    rover.turn_left
    assert_equal rover.d, 'E'

    rover.turn_left
    assert_equal rover.d, 'N'
  end
end
